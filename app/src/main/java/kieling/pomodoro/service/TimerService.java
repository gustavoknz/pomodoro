package kieling.pomodoro.service;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import kieling.pomodoro.utils.Utils;

/*
 * Service to persist timer, even if the app is not being used at that moment
 */
public class TimerService extends Service {
    public static final String COUNTDOWN_PARAMETER = "kieling.pomodoro.countdown";
    private final static String TAG = "TimerService";
    private final Intent intent = new Intent(COUNTDOWN_PARAMETER);
    private CountDownTimer cdt = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Starting timer...");

        cdt = new CountDownTimer(Utils.POMODORO_TIME, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d(TAG, Utils.getTimerTextFromMillis(millisUntilFinished, getApplicationContext()));
                intent.putExtra(Utils.COUNTDOWN_TIME_KEY, millisUntilFinished);
                sendBroadcast(intent);
            }

            @Override
            public void onFinish() {
                Log.d(TAG, "Timer finished");
                intent.putExtra(Utils.COUNTDOWN_TIME_KEY, 0L);
                sendBroadcast(intent);
                stopSelf();
            }
        };
        cdt.start();
    }

    @Override
    public void onDestroy() {
        cdt.cancel();
        Log.d(TAG, "Destroying timer...");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Do not restart service if it is killed
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
