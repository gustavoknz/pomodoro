package kieling.pomodoro.utils;

import android.content.Context;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import kieling.pomodoro.R;

/*
 * Helper class
 */
public class Utils {
    public static final int POMODORO_TIME = 25 * 60 * 1000; //in millis
    public static final String COUNTDOWN_TIME_KEY = "countdown";

    /*
     * Format text to show in timer UI
     */
    public static String getTimerTextFromMillis(long millis, Context context) {
        if (context == null) {
            // Never should happen
            return "Wait...";
        } else {
            return String.format(Locale.getDefault(), context.getString(R.string.timer_text),
                    TimeUnit.MILLISECONDS.toMinutes(millis) % 60,
                    TimeUnit.MILLISECONDS.toSeconds(millis) % 60);
        }
    }
}
