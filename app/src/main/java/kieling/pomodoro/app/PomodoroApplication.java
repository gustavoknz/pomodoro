package kieling.pomodoro.app;

import android.app.Application;

import io.realm.Realm;

/*
 * Initialize database
 */
public class PomodoroApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
