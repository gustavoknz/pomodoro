package kieling.pomodoro.main.history;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import kieling.pomodoro.R;
import kieling.pomodoro.model.EventRealm;
import kieling.pomodoro.model.EventResult;

/*
 * Show all events. Fetch data from database
 */
public class HistoryFragment extends Fragment {
    private static final String TAG = "HistoryFragment";
    private final List<EventResult> mDataSet = new ArrayList<>();
    @BindView(R.id.historyList)
    RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "history onCreate");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "history onCreateView");
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);

        // Use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        loadData();
        mAdapter = new HistoryAdapter(mDataSet, getContext());
        recyclerView.setAdapter(mAdapter);

        return view;
    }

    public void eventAdded() {
        Log.d(TAG, "Reloading data...");
        loadData();
        mAdapter.notifyDataSetChanged();
    }

    private void loadData() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<EventRealm> result = realm.where(EventRealm.class).sort("timestamp").findAll();
        Log.d(TAG, result.size() + " rows found");
        List<EventResult> orderedResult = new ArrayList<>();

        //Only for sorting purpose
        for (EventRealm er : result) {
            orderedResult.add(new EventResult(0, er.getRunningTime(), er.getTimestamp()));
        }
        Collections.sort(orderedResult);
        mDataSet.clear();

        //Using Calendar instead of LocalDate to avoid problems with old Android versions
        int daysAgoToShow;
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTimeInMillis(System.currentTimeMillis());
        Calendar lastCalendar = null;
        Calendar thisCalendar = Calendar.getInstance();
        for (EventResult er : orderedResult) {
            thisCalendar.setTimeInMillis(er.getTimestamp());
            if (lastCalendar != null &&
                    thisCalendar.get(Calendar.DAY_OF_YEAR) == lastCalendar.get(Calendar.DAY_OF_YEAR) &&
                    thisCalendar.get(Calendar.YEAR) == lastCalendar.get(Calendar.YEAR)) {
                daysAgoToShow = -1;
            } else {
                //TODO calculate getActualMaximum for all years, not assuming they are all 365
                daysAgoToShow = currentCalendar.get(Calendar.DAY_OF_YEAR) - thisCalendar.get(Calendar.DAY_OF_YEAR) +
                        (currentCalendar.get(Calendar.YEAR) - thisCalendar.get(Calendar.YEAR)) * 365;
                //thisCalendar.getActualMaximum(Calendar.DAY_OF_YEAR);
            }
            if (lastCalendar == null) {
                lastCalendar = Calendar.getInstance();
            }
            lastCalendar.setTimeInMillis(thisCalendar.getTimeInMillis());
            mDataSet.add(new EventResult(daysAgoToShow, er.getRunningTime(), er.getTimestamp()));
        }
    }
}
