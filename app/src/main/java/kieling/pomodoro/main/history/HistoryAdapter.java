package kieling.pomodoro.main.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import kieling.pomodoro.R;
import kieling.pomodoro.model.EventResult;
import kieling.pomodoro.utils.Utils;

class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private final List<EventResult> mDataSet;
    private final Context mContext;

    // Provide a suitable constructor (depends on the kind of data set)
    HistoryAdapter(List<EventResult> dataSet, Context context) {
        mDataSet = dataSet;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EventResult er = mDataSet.get(position);
        int daysAgo = er.getDaysAgo();
        if (daysAgo == -1) {
            holder.changeDayView.setVisibility(View.GONE);
        } else {
            holder.changeDayView.setVisibility(View.VISIBLE);
            holder.changeDayView.setText(daysAgo == 0 ? mContext.getString(R.string.item_change_day_today)
                    : daysAgo == 1 ? mContext.getString(R.string.item_change_day_yesterday)
                    : mContext.getString(R.string.item_change_day, daysAgo));
        }
        long runningMillis = er.getRunningTime();
        holder.runningTimeView.setText(mContext.getString(R.string.item_running_time,
                TimeUnit.MILLISECONDS.toMinutes(runningMillis),
                TimeUnit.MILLISECONDS.toSeconds(runningMillis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(runningMillis))));
        holder.statusView.setText(Utils.POMODORO_TIME == runningMillis ? R.string.item_status_finished : R.string.item_status_stopped);
        long timestampMillis = System.currentTimeMillis() - er.getTimestamp();
        long days = TimeUnit.MILLISECONDS.toDays(timestampMillis);
        timestampMillis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(timestampMillis);
        timestampMillis-= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(timestampMillis);
        if (days > 0) {
            holder.timestampView.setText(mContext.getString(R.string.item_status_timestamp_days, days));
        } else if (hours > 0) {
            holder.timestampView.setText(mContext.getString(R.string.item_status_timestamp_hours, hours));
        } else {
            holder.timestampView.setText(mContext.getString(R.string.item_status_timestamp_minutes, minutes));
        }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemChangeDay)
        TextView changeDayView;
        @BindView(R.id.itemTotalTime)
        TextView runningTimeView;
        @BindView(R.id.itemStatus)
        TextView statusView;
        @BindView(R.id.itemElapsedTime)
        TextView timestampView;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
