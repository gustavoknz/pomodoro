package kieling.pomodoro.main.timer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import kieling.pomodoro.R;
import kieling.pomodoro.utils.Utils;

/*
 * Show timer (and floating action button)
 */
public class TimerFragment extends Fragment {
    @BindView(R.id.timerText)
    TextView timerText;
    @BindColor(R.color.gray)
    int gray;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;

    public TimerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timer, container, false);
        ButterKnife.bind(this, view);
        setTimer(Utils.POMODORO_TIME);
        return view;
    }

    public void setTimer(long millis) {
        timerText.setText(Utils.getTimerTextFromMillis(millis, getContext()));
        if (millis == Utils.POMODORO_TIME) {
            timerText.setTextColor(gray);
        } else {
            timerText.setTextColor(colorPrimary);
        }
    }
}
