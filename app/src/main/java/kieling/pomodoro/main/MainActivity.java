package kieling.pomodoro.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import kieling.pomodoro.R;
import kieling.pomodoro.main.history.HistoryFragment;
import kieling.pomodoro.main.timer.TimerFragment;
import kieling.pomodoro.model.EventRealm;
import kieling.pomodoro.service.TimerService;
import kieling.pomodoro.utils.Utils;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private final ViewPagerAdapter mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;
    @BindView(R.id.mainPager)
    ViewPager viewPager;
    @BindView(R.id.mainTabs)
    TabLayout tabLayout;
    @BindView(R.id.mainFab)
    FloatingActionButton fab;
    private TimerFragment timerFragment;
    private HistoryFragment historyFragment;
    private boolean started = false;
    private Ringtone ringtone;
    private long millisUntilFinished;

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                millisUntilFinished = intent.getLongExtra(Utils.COUNTDOWN_TIME_KEY, -1L);
                Log.d(TAG, "Countdown seconds remaining: " + millisUntilFinished / 1000);
                if (millisUntilFinished == 0) {
                    started = false;
                    stopTimer();
                } else {
                    timerFragment.setTimer(millisUntilFinished);
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(TimerService.COUNTDOWN_PARAMETER));
        Log.d(TAG, "Broadcast receiver registered");
    }

    @Override
    public void onStop() {
        Log.d(TAG, "Unregistering receiver onStop...");
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            Log.d(TAG, "Error unregistering receiver", e);
        }
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "Starting MainActivity");
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.main_title);
        }

        //Add fragments to adapter
        Log.d(TAG, "savedInstanceState: " + savedInstanceState);
        if (savedInstanceState == null) {
            timerFragment = new TimerFragment();
            historyFragment = new HistoryFragment();
        } else {
            timerFragment = (TimerFragment) mAdapter.getOldFragment(0);
            historyFragment = (HistoryFragment) mAdapter.getOldFragment(1);
        }
        mAdapter.addFragment(timerFragment, getString(R.string.main_fragment_timer));
        mAdapter.addFragment(historyFragment, getString(R.string.main_fragment_history));
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(onTabSelectedListener(viewPager));

        tabLayout.setupWithViewPager(viewPager);
        setFabDrawable();

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
        Log.d(TAG, "Done MainActivity");
    }

    private void setFabDrawable() {
        fab.setImageResource(started ? R.drawable.ic_stop : R.drawable.ic_play);
    }

    @OnClick(R.id.mainFab)
    public void fabClicked() {
        started = !started;
        if (started) {
            startService(new Intent(this, TimerService.class));
            setFabDrawable();
        } else {
            stopService(new Intent(this, TimerService.class));
            stopTimer();
            mainLayout.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_animation));
        }
    }

    private void stopTimer() {
        EventRealm er = new EventRealm(Utils.POMODORO_TIME - millisUntilFinished, System.currentTimeMillis());
        saveInDataBase(er);
        setFabDrawable();
        timerFragment.setTimer(Utils.POMODORO_TIME);
        historyFragment.eventAdded();

        //Play notification sound
        playSound();
    }

    private void playSound() {
        ringtone.play();
    }

    private void saveInDataBase(EventRealm er) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealm(er);
            realm.commitTransaction();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager pager) {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    fab.show();
                } else {
                    fab.hide();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        };
    }

    static final class ViewPagerAdapter extends FragmentPagerAdapter {
        private final FragmentManager mFragmentManager;
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager mFragmentManager) {
            super(mFragmentManager);
            this.mFragmentManager = mFragmentManager;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        Fragment getOldFragment(int position) {
            return mFragmentManager.getFragments().get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
