package kieling.pomodoro.model;

import io.realm.RealmObject;

/*
 * Object used to read and write from/to database, only. It avoids database locking
 */
public class EventRealm extends RealmObject {
    private long runningTime;
    private long timestamp;

    public EventRealm() {
        // Required empty public constructor
    }

    public EventRealm(long runningTime, long timestamp) {
        this.runningTime = runningTime;
        this.timestamp = timestamp;
    }

    public long getRunningTime() {
        return runningTime;
    }

    public void setRunningTime(long runningTime) {
        this.runningTime = runningTime;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "EventRealm{" +
                "runningTime=" + runningTime +
                ", timestamp=" + timestamp +
                '}';
    }
}
