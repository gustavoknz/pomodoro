package kieling.pomodoro.model;

import android.support.annotation.NonNull;

/*
 * Object used to bind data to UI list, sorted by timestamp
 */
public class EventResult implements Comparable<EventResult> {
    private int daysAgo;
    private long runningTime;
    private long timestamp;

    public EventResult(int daysAgo, long runningTime, long timestamp) {
        this.daysAgo= daysAgo;
        this.runningTime = runningTime;
        this.timestamp = timestamp;
    }

    public int getDaysAgo() {
        return daysAgo;
    }

    public void setDaysAgo(int daysAgo) {
        this.daysAgo = daysAgo;
    }

    public long getRunningTime() {
        return runningTime;
    }

    public void setRunningTime(long runningTime) {
        this.runningTime = runningTime;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "EventResult{" +
                "daysAgo=" + daysAgo +
                ", runningTime=" + runningTime +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public int compareTo(@NonNull EventResult er) {
        return (int) (er.timestamp - this.timestamp);
    }
}
